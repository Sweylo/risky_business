-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema risky_business
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `risky_business` ;

-- -----------------------------------------------------
-- Schema risky_business
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `risky_business` DEFAULT CHARACTER SET utf8 ;
USE `risky_business` ;

-- -----------------------------------------------------
-- Table `risky_business`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`users` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(128) NOT NULL,
  `user_email` VARCHAR(128) NOT NULL,
  `user_password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_name_UNIQUE` (`user_name` ASC),
  UNIQUE INDEX `user_email_UNIQUE` (`user_email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`permissions` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`permissions` (
  `permission_level` INT NOT NULL AUTO_INCREMENT,
  `permission_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`permission_level`),
  UNIQUE INDEX `permission_name_UNIQUE` (`permission_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`privileges`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`privileges` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`privileges` (
  `privilege_id` INT NOT NULL AUTO_INCREMENT,
  `privilege_user_id` INT NOT NULL,
  `privilege_permission_level` INT NOT NULL,
  PRIMARY KEY (`privilege_id`),
  INDEX `user_id_idx` (`privilege_user_id` ASC),
  INDEX `permission_level_idx` (`privilege_permission_level` ASC),
  CONSTRAINT `user_id`
    FOREIGN KEY (`privilege_user_id`)
    REFERENCES `risky_business`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `permission_level`
    FOREIGN KEY (`privilege_permission_level`)
    REFERENCES `risky_business`.`permissions` (`permission_level`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`logs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`logs` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`logs` (
  `log_id` INT NOT NULL AUTO_INCREMENT,
  `log_type` VARCHAR(50) NOT NULL,
  `log_code` INT NOT NULL DEFAULT 0,
  `log_description` LONGTEXT NOT NULL,
  `log_time` DATETIME NOT NULL DEFAULT NOW(),
  `log_user_id` INT NOT NULL,
  `log_referer_url` MEDIUMTEXT NOT NULL,
  PRIMARY KEY (`log_id`),
  UNIQUE INDEX `log_id_UNIQUE` (`log_id` ASC),
  INDEX `log_user_id_idx` (`log_user_id` ASC),
  CONSTRAINT `log_user_id`
    FOREIGN KEY (`log_user_id`)
    REFERENCES `risky_business`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`maps`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`maps` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`maps` (
  `map_id` INT NOT NULL AUTO_INCREMENT,
  `map_name` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`map_id`),
  UNIQUE INDEX `map_name_UNIQUE` (`map_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`games`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`games` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`games` (
  `game_id` INT NOT NULL AUTO_INCREMENT,
  `game_name` VARCHAR(128) NOT NULL,
  `game_map_id` INT NOT NULL,
  PRIMARY KEY (`game_id`),
  UNIQUE INDEX `game_id_UNIQUE` (`game_id` ASC),
  UNIQUE INDEX `game_name_UNIQUE` (`game_name` ASC),
  INDEX `game_map_id_idx` (`game_map_id` ASC),
  CONSTRAINT `game_map_id`
    FOREIGN KEY (`game_map_id`)
    REFERENCES `risky_business`.`maps` (`map_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`lobby`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`lobby` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`lobby` (
  `lobby_id` INT NOT NULL AUTO_INCREMENT,
  `lobby_game_id` INT NOT NULL,
  `lobby_user_id` INT NOT NULL,
  `lobby_permission_level` INT NOT NULL,
  PRIMARY KEY (`lobby_id`),
  INDEX `lobby_game_id_idx` (`lobby_game_id` ASC),
  INDEX `lobby_user_id_idx` (`lobby_user_id` ASC),
  INDEX `lobby_permission_id_idx` (`lobby_permission_level` ASC),
  CONSTRAINT `lobby_game_id`
    FOREIGN KEY (`lobby_game_id`)
    REFERENCES `risky_business`.`games` (`game_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lobby_user_id`
    FOREIGN KEY (`lobby_user_id`)
    REFERENCES `risky_business`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lobby_permission_level`
    FOREIGN KEY (`lobby_permission_level`)
    REFERENCES `risky_business`.`permissions` (`permission_level`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`map_permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`map_permissions` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`map_permissions` (
  `map_permission_id` INT NOT NULL AUTO_INCREMENT,
  `map_permission_user_id` INT NOT NULL,
  `map_permission_map_id` INT NOT NULL,
  `map_permission_level` INT NOT NULL,
  PRIMARY KEY (`map_permission_id`),
  INDEX `map_permission_user_id_idx` (`map_permission_user_id` ASC),
  INDEX `map_permission_level_idx` (`map_permission_level` ASC),
  INDEX `map_permission_map_id_idx` (`map_permission_map_id` ASC),
  CONSTRAINT `map_permission_user_id`
    FOREIGN KEY (`map_permission_user_id`)
    REFERENCES `risky_business`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `map_permission_level`
    FOREIGN KEY (`map_permission_level`)
    REFERENCES `risky_business`.`permissions` (`permission_level`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `map_permission_map_id`
    FOREIGN KEY (`map_permission_map_id`)
    REFERENCES `risky_business`.`maps` (`map_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`countries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`countries` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`countries` (
  `country_id` INT NOT NULL AUTO_INCREMENT,
  `country_name` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`country_id`),
  UNIQUE INDEX `country_name_UNIQUE` (`country_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`continents`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`continents` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`continents` (
  `continent_id` INT NOT NULL AUTO_INCREMENT,
  `continent_name` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`continent_id`),
  UNIQUE INDEX `continent_name_UNIQUE` (`continent_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`linking`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`linking` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`linking` (
  `linking_id` INT NOT NULL AUTO_INCREMENT,
  `linking_map_id` INT NOT NULL,
  `linking_country_id` INT NOT NULL,
  `linking_continent_id` INT NOT NULL,
  PRIMARY KEY (`linking_id`),
  INDEX `linking_map_id_idx` (`linking_map_id` ASC),
  INDEX `linking_country_id_idx` (`linking_country_id` ASC),
  INDEX `linking_continent_id_idx` (`linking_continent_id` ASC),
  CONSTRAINT `linking_map_id`
    FOREIGN KEY (`linking_map_id`)
    REFERENCES `risky_business`.`maps` (`map_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `linking_country_id`
    FOREIGN KEY (`linking_country_id`)
    REFERENCES `risky_business`.`countries` (`country_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `linking_continent_id`
    FOREIGN KEY (`linking_continent_id`)
    REFERENCES `risky_business`.`continents` (`continent_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `risky_business`.`connections`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `risky_business`.`connections` ;

CREATE TABLE IF NOT EXISTS `risky_business`.`connections` (
  `connection_id` INT NOT NULL AUTO_INCREMENT,
  `connection_country_1_id` INT NOT NULL,
  `connection_country_2_id` INT NOT NULL,
  PRIMARY KEY (`connection_id`),
  INDEX `connection_country_1_id_idx` (`connection_country_1_id` ASC),
  INDEX `connection_country_2_id_idx` (`connection_country_2_id` ASC),
  CONSTRAINT `connection_country_1_id`
    FOREIGN KEY (`connection_country_1_id`)
    REFERENCES `risky_business`.`countries` (`country_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `connection_country_2_id`
    FOREIGN KEY (`connection_country_2_id`)
    REFERENCES `risky_business`.`countries` (`country_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE = '';
GRANT USAGE ON `risky_business`.* TO risky_business_user;
 DROP USER risky_business_user;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'risky_business_user' IDENTIFIED BY 'bobseger';

GRANT SELECT, INSERT, TRIGGER ON TABLE `risky_business`.* TO 'risky_business_user';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE `risky_business`.* TO 'risky_business_user';
GRANT SELECT ON TABLE `risky_business`.* TO 'risky_business_user';
GRANT ALL ON `risky_business`.* TO 'risky_business_user';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `risky_business`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `risky_business`;
INSERT INTO `risky_business`.`users` (`user_id`, `user_name`, `user_email`, `user_password`) VALUES (DEFAULT, 'admin', 'admin@localhost', 'password');

COMMIT;


-- -----------------------------------------------------
-- Data for table `risky_business`.`permissions`
-- -----------------------------------------------------
START TRANSACTION;
USE `risky_business`;
INSERT INTO `risky_business`.`permissions` (`permission_level`, `permission_name`) VALUES (DEFAULT, 'basic');
INSERT INTO `risky_business`.`permissions` (`permission_level`, `permission_name`) VALUES (DEFAULT, 'moderator');
INSERT INTO `risky_business`.`permissions` (`permission_level`, `permission_name`) VALUES (DEFAULT, 'admin');
INSERT INTO `risky_business`.`permissions` (`permission_level`, `permission_name`) VALUES (DEFAULT, 'owner');

COMMIT;