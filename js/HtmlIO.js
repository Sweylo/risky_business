HtmlIO = function(id, parentId, defaultValue, attrs, isNumeric = false) {
	
	// fields
	var id = id;
	var parent = document.getElementById(parentId);
	var attrs = attrs;
	var isNumeric = isNumeric;
	var handle;
	
	// append the input element to the parent element
	$(parent).append('<input id="' + id + '" value="' + defaultValue + '">');
	
	// get the handle
	handle = document.getElementById(id);
	
	// set the attributes for the element
	for (var key in attrs) {
		if (attrs.hasOwnProperty(key)) {
			$(handle).attr(key, attrs[key]);
		}
	}
	
	this.get = function() {
		if (isNumeric) {
			return Number($(handle).val());
		} else {
			return $(handle).val();
		}
	};
	
	this.set = function(value) {
		if (isNumeric) {
			$(handle).val(Number(value));
		} else {
			$(handle).val(value);
		}
	};
	
};