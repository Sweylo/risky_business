// globals
var canvas;
var gl;
var shaderProgram;
var ratio;
var mat4 = mat4;
var mvMatrix = mat4.create();
var pMatrix = mat4.create();
var mvMatrixStack = [];

/**
 * class for handling the drawing of shapes based on an array of vertices using webgl
 *
 * @param {array} vertices array of coordinates [x1, y1, x2, y2, ... xn, yn]
 * @param {int} drawMode GL draw modes, eg: gl.TRIANGLES, gl.LINE_LOOP, gl.TRIANGLE_FAN
 * @param {array} color 3 element array of color values: [red, green, blue]
 * @returns {Shape}
 */
Shape = function(vertices, drawMode, color) {

	// fields
	this.vertices = vertices;
	this.drawMode = drawMode;
	this.color = color;

	/*
	 * member functions
	 */

	this.init = function() {

		// get the number of rows of vertices
		this.numItems = this.vertices.length / Shape.itemSize;

		// copy the color array to fit the number of vertices
		this.colors = this.color;
		for (var i = 1; i < this.numItems; i++) {
			this.colors = this.colors.concat(this.color);
		}

		if (gl && shaderProgram) {

			// setup color buffer
			this.verticesColorBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this.verticesColorBuffer);
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.colors), gl.STATIC_DRAW);

			// setup vertex buffer
			this.vertexPositionBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.vertices), gl.STATIC_DRAW);

		} else {
			console.log('webgl and shaders not initialized');
			return null;
		}

	};

	this.draw = function() {

		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute,
			Shape.itemSize, gl.FLOAT, false, 0, 0);

		gl.bindBuffer(gl.ARRAY_BUFFER, this.verticesColorBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexColorAttribute,
			3, gl.FLOAT, false, 0, 0);

		setMatrixUniforms();
		gl.drawArrays(this.drawMode, 0, this.numItems);

	};

	this.isPointInside = function(x, y) {

		var glCoords = getPoint(x, y);

		// this is gonna take some time

		return false;

	};

	// initialize this object
	this.init();

};

// initialize static fields
Shape.itemSize = 2;			// use 2d coordinates by default

// initialize the gl object
function initGL() {
	try {
		gl = canvas.getContext("experimental-webgl");
		gl.viewportWidth = canvas.width;
		gl.viewportHeight = canvas.height;
	} catch (e) {
		console.log(e.toString());
		alert("Could not initialize WebGL");
	}
}

// get a given shader by html id
function getShader(id) {

	var shaderScript = document.getElementById(id);

	if (!shaderScript) {
		return null;
	}

	var str = "";
	var k = shaderScript.firstChild;

	while (k) {

		if (k.nodeType === 3) {
			str += k.textContent;
		}

		k = k.nextSibling;

	}

	var shader;

	if (shaderScript.type === "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type === "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		return null;
	}

	gl.shaderSource(shader, str);
	gl.compileShader(shader);

	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return null;
	}

	return shader;

}

function initShaders() {

	var fragmentShader = getShader("shader-fs");
	var vertexShader = getShader("shader-vs");
	//var vertexShader = getShader("2d-vertex-shader");
	shaderProgram = gl.createProgram();

	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Could not initialise shaders");
	}

	gl.useProgram(shaderProgram);

	shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
	//console.log(shaderProgram.vertexPositionAttribute);
	gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

	shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, 'aVertexColor');
	gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);

	shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
	shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");

	// set the resolution
	//var resolutionLocation = gl.getUniformLocation(shaderProgram, "u_resolution");
	//gl.uniform3f(resolutionLocation, canvas.clientWidth, canvas.clientHeight, 1.0);
	//gl.enableVertexAttribArray(resolutionLocation);

}

function setMatrixUniforms() {
	gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
	gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

function mvPushMatrix() {
	var copy = mat4.create();
	mat4.set(mvMatrix, copy);
	mvMatrixStack.push(copy);
}

function mvPopMatrix() {

	if (mvMatrixStack.length === 0) {
		throw "Invalid popMatrix!";
	}

	mvMatrix = mvMatrixStack.pop();

}

function degToRad(deg) {
	return deg * Math.PI / 180;
}

function getPoint(x, y) {

	ratio = Number($('#zoom').val()) / canvas.height;

	var pointX = (x - canvas.width / 2) * ratio;
	var pointY = (y - canvas.height / 2) * ratio;
	var shiftX = (Number($('#transX').val())) * ratio;
	var shiftY = (Number($('#transY').val())) * ratio;

	//console.log('shift | (' + shiftX + ', ' + shiftY + ')');

	return [pointX - shiftX, pointY - shiftY];

}

function round(value, decimals) {
  	return Number(
		Math.round(value + 'e' + decimals) + 'e-' + decimals
	);
}
