class Square {

	constructor(center, drawMode, color, size) {

		this.center = center;
		this.drawMode = drawMode;
		this.color = color;
		this.size = size;

		this.init();
		
	}

	init() {

		// assign the vertices of the square based on the center point and size
		this.vertices = [
			this.center[0] + this.size, this.center[1] + this.size,
			this.center[0] + this.size, this.center[1] - this.size,
			this.center[0] - this.size, this.center[1] - this.size,
			this.center[0] - this.size, this.center[1] + this.size,
		];

		try {
			this.shape = new Shape(this.vertices, this.drawMode, this.color);
		} catch (e) {
			alert('Unable to instantiate Shape');
			console.log(e);
		}

	};

	draw() {
		this.shape.draw();
	};

}
