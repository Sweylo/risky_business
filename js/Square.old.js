/**
 * class that 'extends' the Shape class for use of drawing squares given the center point
 */
Square = function(center, drawMode, color, size) {

	// fields
	this.center = center;
	this.drawMode = drawMode;
	this.color = color;
	this.size = size;

	/*
	 * member functions
	 */

	this.init = function() {

		// assign the vertices of the square based on the center point and size
		this.vertices = [
			this.center[0] + this.size, this.center[1] + this.size,
			this.center[0] + this.size, this.center[1] - this.size,
			this.center[0] - this.size, this.center[1] - this.size,
			this.center[0] - this.size, this.center[1] + this.size,
		];

		try {
			this.shape = new Shape(this.vertices, this.drawMode, this.color);
		} catch (e) {
			alert('Unable to instantiate Shape');
			console.log(e);
		}

	};

	this.draw = function() {
		this.shape.draw();
	};

	this.isPointInside = function(x, y) {

		var glCoords = getPoint(x, y);

		// this is gonna take some time

		return false;

	};

	// initialize this object
	this.init();

};
