<?php

/**
 *	controller for the main page
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);
date_default_timezone_set('UTC');

session_start();

$page = 'home';

require_once('../model/sql.php');
require_once('../model/log.php');
require_once('../model/err.php');
require_once('../model/input.php');
require_once('../model/user_db.php');

$action = (input(INPUT_GET, 'action', true) === null) 
	? input(INPUT_POST, 'action', true) 
	: input(INPUT_GET, 'action', true);

switch ($action) {

	case 'login': 
	
		$username = input(INPUT_POST, 'user_name');
		$password = input(INPUT_POST, 'user_password');

		if ($username == null || $username == false || $password == null || $password == false) {
			$error = 'invalid username/password (probably left blank)';
			include('../view/error.php');
			die();
		}

		if (validate_user($username, $password)) {
			$_SESSION['user'] = $username;
			header("Location: $_SERVER[HTTP_REFERER]");
		} else {
			$error = 'incorrect username/password';
			include('../view/error.php');
			die();
		}

		break;
	
	case 'logout':
	
		session_unset();
		session_destroy();
		header("Location: $_SERVER[HTTP_REFERER]");

		break;
	
	case 'register':
	
		$register = true;

		// prevent a logged in user from accessing register page
		if ($me) {
			header('Location: .');
		} else {
			include('register.php');
		}
		
		break;
	
	case 'add_user':
	
		$username = filter_input(INPUT_POST, 'user_name');
		$password = filter_input(INPUT_POST, 'user_password');
		$confirm = filter_input(INPUT_POST, 'confirm');
		$email = filter_input(INPUT_POST, 'user_email', FILTER_VALIDATE_EMAIL);

		if (
			$username === null || $username === false || 
			$password === null || $password === false ||
			$confirm === null || $confirm === false ||
			$email === null || $email === false
		) {
			die('invalid data');
		} else if ($password != $confirm) {
			die('passwords did not match');
		}

		add_user($username, sha1($username . $password), $email);

		header('Location: .');
		
		break;
		
	default:
		include('home.php');
	
}

?>