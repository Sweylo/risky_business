<?php include('../view/header.php'); ?>

<!-- handles drawing of shapes and mouse controls -->
<script src="../map/mapCreator.js"></script>
<!-- handles keyboard controls -->
<script src="../map/jquery.map_control.js"></script>

<div id="map">
	
	<input id="transX" type="hidden" name="transX" value="0">
	<input id="transY" type="hidden" name="transY" value="0">
	<input id="rotX" type="hidden" name="rotX" value="0" disabled>
	<input id="rotY" type="hidden" name="rotY" value="0" disabled>
	<input id="zoom" type="hidden" name="zoom" min="1" max="100" value="6">
	<input id="fov" type="hidden" name="fov" min="1" value="53" disabled>
	
	<h4>Loading...</h4>
	
	<canvas id="map-canvas"></canvas>
	
	<div class="control" id="primary">
		
		<div class="control-header">
			<h4>Map Creator</h4>
			<button class="hide off">Show</button>
			<div class="clear"></div>
		</div>
		
		<div class="control-content">
			
			<table>
				
				<tr>
					<td>gridSize</td>
					<td id='gridSizeParent'></td>
				</tr>
				
			</table>
			
			<button id="reset">Reset</button>
			
		</div>
		
	</div>
	
	<div id="ajax" class="debug"></div>
	
</div>

<?php include('../view/footer.php'); ?>