<?php

/**
 *	controller for the map pages
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);
date_default_timezone_set('UTC');

$page = 'map';
$init_webgl = true;

require_once('../model/sql.php');
require_once('../model/err.php');
require_once('../model/input.php');
require_once('../model/user_db.php');

$action = (input(INPUT_GET, 'action', true) === null) 
	? input(INPUT_POST, 'action', true) 
	: input(INPUT_GET, 'action', true);

// display error if not admin user
if (sql::is_connected() && $me['user_name'] != 'admin' && !$admin_needs_pw) {
	$action = 'unauthorized';
}

switch ($action) {

	case 'create_map_form': default:
		include('create_map_form.php');
		break;
	
	
		
	
}

?>