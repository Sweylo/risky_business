/**
 * jQuery map controls
 */

$(document).ready(function() {

	// resets tranformation variables by clicking the reset button
	$('#reset').click(function() {

		console.log('reset');

		$('#transX').val(0);
		$('#transY').val(0);
		$('#rotX').val(0);
		$('#rotY').val(0);
		$('#fov').val(53);	// 53 looks good on my laptop
		$('#zoom').val(6);	// 6 looks good on my laptop

		squares = [];

	});

	// keyboard controls
	$(document).keydown(function(e) {

		var step = 20;

		//console.log(e.which);

		switch(e.which) {

			case 37: // left
				console.log('left');
				$('#transX').val(parseInt($('#transX').val()) - step);
				break;

			case 38: // up arrow key
				console.log('up');
				$('#transY').val(parseInt($('#transY').val()) + step);
				break;

			case 39: // right arrow key
				console.log('right');
				$('#transX').val(parseInt($('#transX').val()) + step);
				break;

			case 40: // down arrow key
				console.log('down');
				$('#transY').val(parseInt($('#transY').val()) - step);
				break;

			case 187: // '+' key
				console.log('zoom in');
				$('#zoom').val(parseInt($('#zoom').val()) - 1);
				break;

			case 189: // '-' key
				console.log('zoom out');
				$('#zoom').val(parseInt($('#zoom').val()) + 1);
				break;

			default:
				return;

		}

		e.preventDefault(); // prevent the default action (scroll / move caret)

	});

	$('.hide').click(function() {

		//console.log($(this).parent().siblings('.control-content'));
		var aniSpeed = 500;	// animation speed in milliseconds

		if ($(this).hasClass('on')) {
			$(this).removeClass('on');
			$(this).addClass('off');
			$(this).parent().siblings('.control-content').show(aniSpeed);
			$(this).html('Hide');
		} else if ($(this).hasClass('off')) {
			$(this).removeClass('off');
			$(this).addClass('on');
			$(this).parent().siblings('.control-content').hide(aniSpeed);
			$(this).html('Show');
		}

	});

	$('#loadTest').click(function() {

		// makes an HTTP request sending the JSON data via POST
		$('#ajax').load('../webgl_test/?action=load_test', {
			test_col: 'jive'
		});

	});

});
