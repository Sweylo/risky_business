// globals
var thePast = 0;
var mouseDown = false;
var lastMouseX = 0;
var lastMouseY = 0;
var gridArray = [];
var squares = [];

// global shapes
var cursor;
var grid;

// global control variables
var gridSize = new HtmlIO();

// globals inherited from Shape.js (must be called before this)
var canvas = canvas;
var gl = gl;
var ratio = ratio;
var mat4 = mat4;
var mvMatrix = mvMatrix;
var pMatrix = pMatrix;

// constants
const MOUSE_LEFT = 1;
const MOUSE_RIGHT = 2;
const MOUSE_MIDDLE = 4;
const ASIDE_WIDTH = 241;
const GRID_X_MIN = -10;
const GRID_X_MAX = 10;
const GRID_Y_MIN = -10;
const GRID_Y_MAX = 10;

// draw the objects
function drawScene() {

	// calculate cursor location
	var cursorLoc = getCursorLoc(getPoint(lastMouseX - ASIDE_WIDTH, canvas.height - lastMouseY));

	initGrid();

	/*
	 * initialize objects to be drawn
	 */

	cursor = new Square(
		[cursorLoc[0], cursorLoc[1]],
	gl.TRIANGLE_FAN, [0.95, 0.95, 0.95], gridSize.get());

	grid = new Shape(gridArray, gl.LINES, [0.9, 0.9, 0.9]);

	/*
	 * draw the objects
	 */

	// clear the screen
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// resize canvas accordingly
	resize();

	// get values from the control form
	var transX = $('#transX').val();
	var transY = $('#transY').val();
	//var rotX = $('#rotX').val();
	//var rotY = $('#rotY').val();
	var zoom = -$('#zoom').val();
	var fov = $('#fov').val();

	// set the viewport dimensions
	gl.viewport(0, 0, canvas.width, canvas.height);

	// setup the camera
	mat4.perspective(
		fov,									// field of view
		canvas.width / canvas.height,			// aspect ratio
		0.1,									// z-near
		100.0,									// z-far
		pMatrix
	);

	// initialize the matrix stack
	mat4.identity(mvMatrix);

	// perform tranformations
	mat4.translate(mvMatrix, [transX * ratio, transY * ratio, zoom]);

	// try to draw the objects
	try {

		mvPushMatrix();

			cursor.draw();
			grid.draw();

			for (var i = 0; i < squares.length; i++) {

				if (squares[i].size !== gridSize.get()) {
					squares[i].size = gridSize.get();
					squares[i].init();
				}

				squares[i].draw();

			}

		mvPopMatrix();

	} catch (e) {
		//console.log('error drawing shapes');
		console.log(e);
	}

}

function resize() {

	// check if the canvas is not the same size
	if (canvas.width !== canvas.clientWidth || canvas.height !== canvas.clientHeight) {

		canvas.width = canvas.clientWidth;
		canvas.height = canvas.clientHeight;

		ratio = parseFloat($('#zoom').val()) / canvas.height;

	}

	//console.log('size | (' + canvas.width + ', ' + canvas.height + ')');

	initGL();

}

function handleMouseDown(event) {

	mouseDown = event.buttons;
	lastMouseX = event.clientX;
	lastMouseY = event.clientY;

	var point = getPoint(lastMouseX - ASIDE_WIDTH, canvas.height - lastMouseY);
	var cursorCenter = getCursorLoc(point);
	var currentCenter;

	//console.log(event);

	switch (mouseDown) {

		case MOUSE_LEFT:

			// duplicate check
			for (var i = 0; i < squares.length; i++) {

				try {
					currentCenter = squares[i].center;
				} catch(e) {
					currentCenter = [null, null];
				}

				console.log(currentCenter);

				if (currentCenter === cursorCenter) {
					console.log('duplicate');
				}

			}

			// add new shape at location of the cursor
			squares = squares.concat(new Square(
				[round(cursorCenter[0], 3), round(cursorCenter[1], 3)],
			gl.TRIANGLE_FAN, [0.9, 0.9, 0.9], gridSize.get()));

			break;

		case MOUSE_RIGHT:
			canvas.style.cursor = "move";
			break;

	}

	console.log('cursor-center | ('
		+ round(cursorCenter[0], 3)
		+ ', '
		+ round(cursorCenter[1], 3)
		+ ')'
	);
	console.log(squares);
	console.log("click | (" + point[0] + ", " + point[1] + ")");

}

function handleMouseUp(event) {

	canvas.style.cursor = "pointer";
	mouseDown = false;

	//console.log(test);
	//console.log(testArray);

}

function handleMouseMove(event) {

	if (!mouseDown) {
		//return;
	}

	var newX = event.clientX;
	var newY = event.clientY;
	var deltaX = newX - lastMouseX;
	var deltaY = newY - lastMouseY;

	//console.log("delta | (" + deltaX + ", " + deltaY + ")");

	switch (mouseDown) {

		case MOUSE_RIGHT:
			$('#transX').val(parseInt($('#transX').val()) + deltaX);
			$('#transY').val(parseInt($('#transY').val()) - deltaY);
			break;

	}

	lastMouseX = newX;
	lastMouseY = newY;

}

function handleMouseScroll(event) {

	//console.log(event);

	var step = 0.5;

	switch(event.deltaY) {

		case -100:	// scroll up
			if (parseInt($('#zoom').val()) > 0) {
				$('#zoom').val(parseFloat($('#zoom').val()) - step);
			}
			break;

		case 100:	// scroll down
			if (parseInt($('#zoom').val()) < 99) {
				$('#zoom').val(parseFloat($('#zoom').val()) + step);
			}
			break;
	}

}

// generates the array used to draw the grid
function initGrid() {

	var grid = [];
	var gridStep = gridSize.get() * 2;

	// traverse the x-axis
	for (var i = GRID_X_MIN; i < GRID_X_MAX; i += gridStep) {
		grid = grid.concat([i, GRID_Y_MAX, i, GRID_Y_MIN]);
	}

	// traverse the y-axis
	for (var i = GRID_Y_MIN; i < GRID_Y_MAX; i += gridStep) {
		grid = grid.concat([GRID_X_MAX, i, GRID_X_MIN, i]);
	}

	gridArray = grid;

}

function initHtmlControls() {

	gridSize = new HtmlIO('gridSize', 'gridSizeParent', 0.1, {
		type: 'number',
		step: 0.1,
		min: 0.1
	}, true);

}

function getCursorLoc(mousePoint) {

	var dirX = (mousePoint[0] > 0) ? 1 : -1;
	var dirY = (mousePoint[1] > 0) ? 1 : -1;
	var cursorStep = parseFloat(gridSize.get()) * 2;
	var i;
	var j;

	for (i = 0; i <= Math.abs(mousePoint[0]); i += cursorStep);
	for (j = 0; j <= Math.abs(mousePoint[1]); j += cursorStep);

	return [
		parseFloat(i * dirX - parseFloat(gridSize.get()) * dirX),
		parseFloat(j * dirY - parseFloat(gridSize.get()) * dirY)
	];

}

// performs increments of animation variables
function animate() {

	var thePresent = new Date().getTime();

	if (thePast !== 0) {

	}

	thePast = thePresent;

}

// responsible for animation
function tick() {
	requestAnimationFrame(tick);
	drawScene();
	animate();
}

// calls all functions to get webgl running
function webGLStart() {

	// get canvas object from the html element
	canvas = document.getElementById("map-canvas");

	// set cursor to indicate loading
	canvas.style.cursor = 'wait';

	// initialization
	initGL();
	initShaders();
	initHtmlControls();
	initGrid();

	// disable right click menu
	$(canvas).bind('contextmenu', function(e) {
		return false;
	});

	// prevent default action for middle mouse button
	$(canvas).on('click', function(e) {
		if(e.which === 2) {
			e.preventDefault();
		}
	});

	// show controls and set cursor to pointer
	canvas.style.cursor = "pointer";
	$('.control').show();

	// set background color and enable depth testing
	gl.clearColor(1.0, 1.0, 1.0, 1.0);
	gl.enable(gl.DEPTH_TEST);

	// assign event handling functions
	canvas.onmousedown = handleMouseDown;
    canvas.onmouseup = handleMouseUp;
    canvas.onmousemove = handleMouseMove;
	canvas.addEventListener('wheel', handleMouseScroll, false);

	tick();

}
