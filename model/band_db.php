<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", 1);
date_default_timezone_set('UTC');

function get_bands() {
	
	global $db;
	
	$sql = 'SELECT *
			FROM bands
			ORDER BY band_name';
	
	$statement = $db -> prepare($sql);
	$statement -> execute();
	$bands = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $bands;
	
}

function get_band_by_id($band_id) {
	
	global $db;
	
	$sql = 'SELECT *
			FROM bands
			WHERE band_id = :band_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':band_id', $band_id);
	$statement -> execute();
	$band = $statement -> fetch();
	$statement -> closeCursor();
	
	return $band;
	
}

function add_band($creator_user_id, $bandname, $description, $zip) {
	
	global $db;
	
	// add the record to the bands table
	$sql = 'INSERT INTO bands (band_name, band_description, band_zip) 
			VALUES (:band_name, :band_description, :band_zip)';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':band_name', $bandname);
	$statement -> bindValue(':band_description', $description);
	$statement -> bindValue(':band_zip', $zip);
	$statement -> execute();
	
	// get the id of the newly created record
	$new_band_id = $db -> lastInsertId();
	
	// add a record in the permissions table giving ownership to the creating user 
	$sql = 'INSERT INTO band_permissions (user_id, band_id, permission_level) 
			VALUES (:user_id, :band_id, 4)';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':user_id', $creator_user_id);
	$statement -> bindValue(':band_id', $new_band_id);
	$statement -> execute();
	
	$statement -> closeCursor();
	
}

function get_band_permission_level($user_id, $band_id) {
	
	global $db;
	
	// get users that have permission
	$sql = 'SELECT * 
			FROM bands b JOIN band_permissions bp 
				ON b.band_id = bp.band_id 
			WHERE b.band_id = :band_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':band_id', $band_id);
	$statement -> execute();
	$permissions = $statement -> fetchAll();
	
	// default permission level is 0 (no permission)
	$permission_level = 0;
	
	// check the joined rows for the user's id and get the permission level of said user
	foreach ($permissions as $permission) {
		if ($permission['user_id'] == $user_id) {
			$permission_level = $permission['permission_level'];
			break;
		}
	}
	
	$statement -> closeCursor();
	
	return $permission_level;
	
}

function get_bands_by_user($user_id, $permission_level) {
	
	global $db;
	
	$sql = 'SELECT *
			FROM bands b JOIN band_permissions bp
				ON b.band_id = bp.band_id
			WHERE bp.user_id = :user_id
				AND permission_level >= :permission_level';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':user_id', $user_id);
	$statement -> bindValue(':permission_level', $permission_level);
	$statement -> execute();
	$bands = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $bands;
	
}

function save_band($band_id, $bandname, $description, $zip) {
	
	global $db;
	
	$sql = 'UPDATE bands 
			SET band_name = :band_name,
				band_description = :band_description,
				band_zip = :band_zip 
			WHERE band_id = :band_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':band_id', $band_id);
	$statement -> bindValue(':band_name', $bandname);
	$statement -> bindValue(':band_description', $description);
	$statement -> bindValue(':band_zip', $zip);
	$statement -> execute();
	
}

function delete_band($band_id) {
	
	global $db;
	
	// add the record to the bands permission table
	$sql = 'DELETE FROM band_permissions
			WHERE band_id = :band_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':band_id', $band_id);
	$statement -> execute();
	
	// add the record to the bands table
	$sql = 'DELETE FROM bands 
			WHERE band_id = :band_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':band_id', $band_id);
	$statement -> execute();
	
}

?>