<?php

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

class db extends mysqli {
	
	public static $error_message;

    public function __construct($config) {
		
		global $is_db_setup;
		
		if ($config) {
			
			try {
				
				parent::__construct(
					$config['mysql_host'],
					$config['mysql_user'],
					$config['mysql_password'],
					$config['mysql_db'],
					$config['mysql_port']
				);
				
			} catch (mysqli_sql_exception $e) {
				
				if (isset($is_db_setup) && $is_db_setup) {
					db::$error_message = $e->getMessage();
				} else {
					header("Location: ../setup/?action=db_setup");
				}
				
				throw $e;
				
			}
			
		} else {
			throw new Exception('no config');
		}
		
    }

};

// read in settings from the config file
if (isset($is_db_setup) && $is_db_setup) {
	$config = (file_exists("../config/db_config.json"))
		? json_decode(file_get_contents("../config/db_config.json"), true)
		: null;
} else {
	$config = json_decode(file_get_contents("../config/db_config.json"), true)
		or header("Location: ../setup/?action=db_setup");
}

// try to connect to the database
try {
	$db = new db($config);
} catch (Exception $e) {
	$db = null;
}


?>
