<?php

class err {
	
	public static function out($error, $redirect = true) {
		
		//log::error($error);
		
		if ($redirect) {
			include('../view/error.php');
		} else {
			echo $error;
		}
		
		die();
		
	}
	
};

?>