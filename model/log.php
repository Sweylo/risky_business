<?php

require_once('../model/sql.php');
require_once('../model/input.php');
require_once('../model/user_db.php');

class log {	

	public static function error($message, $code = 0) {
		
		global $me;
		
		$log['log_type'] = 'error';
		$log['log_code'] = $code;
		$log['log_description'] = $message;
		$log['log_time'] = time();
		$log['log_user_id'] = $me['user_id'] ? $me['user_id'] : 0;
		$log['log_referer_url'] = input(INPUT_SERVER, 'HTTP_REFERER');
			
		sql::insert('logs', $log);
		
	}
	
	public static function get_logs() {
		
	}
 	
};

?>