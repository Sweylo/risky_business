<?php

/**
 * SQL helper class to make moving data between the pages and db easier
 * Extends the ArrayObject class allowing the object to be treated like an array
 */

class sql extends ArrayObject {

	// class fields
	public $table;
	public $data;
	
	/**
	 * Creates new sql helper object
	 * 
	 * @param type $table name of the table in the database
	 * @param type $data the row(s) of the table
	 */
	public function __construct($table, $data = array()) {
		parent::__construct();
		$this->table = $table;
		$this->data = $data;
	}
	
	/**
	 * Retrieves data from the sql table using a SELECT clause. Use the column/value parameters as 
	 * the 'WHERE' part of the SQL statement. If no parameters are passed, all rows will be 
	 * returned.
	 * 
	 * @param type $column column/field name
	 * @param type $value value of the column
	 * @param type $limit value of the LIMIT clause
	 * @throws Exception
	 */
	public function select($column = null, $value = null, $limit = null) {

		global $db;
		
		// get all rows
		if ($column == null && $value == null && $limit == null) {
			
			$sql = "SELECT * FROM $this->table";
			$params = null;
		
		// get specified rows using WHERE clause
		} else if ($column != null && $value != null && $limit == null) {
			
			$sql = 'SELECT * FROM ? WHERE ? = ?';
			$params = array($this->table, $column, $value);
		
		// get all rows using a limit
		} else if ($column == null && $value == null && $limit != null) {
			
			$sql = "SELECT * FROM $this->table LIMIT ?";
			$params = array($limit);
		
		// get specified rows using WHERE clause and a limit	
		} else if ($column != null && $value != null && $limit != null) {
			
			$sql = 'SELECT * FROM ? WHERE ? = ? LIMIT ?';
			$params = array($this->table, $column, $value, $limit);
		
		} else {
			throw new Exception('missing parameter');
		}
		
		// prepare the statement
		$stmt = $db->prepare($sql);
		
		// bind parameters if there are any
		if (!empty($params)) {
			foreach ($params as $param) {
				$stmt->bind_param(sql::get_db_var_type($param), $param);
			}
		}
		
		// execute the query and store the result
		$stmt->execute();
		$result = $stmt->get_result();
		
		// push each row of the result as a new sql object to the local array
		$data = array();
		while ($row = $result->fetch_array()) {
			array_push($data, new sql($this->table, $row));
		}
		
		// close the prepared statement
		$stmt->close();
		
		// assign the local array to the global array
		$this->data = $data;
		
	}
	
	public static function insert($table, $data) {
		
		global $db;
		
		foreach ($data as $i => $datum) {
			$columns .= "$i, ";
			$values .= "'$datum', ";
		}
		
		// trim the tailing comma and space from each string
		$columns = substr($columns, 0, strlen($columns) - 2);
		$values = substr($values, 0, strlen($values) - 2);
		
		$sql = "INSERT INTO $table ($columns) VALUES ($values)";
		
		// prepare the statement and bind the table parameter
		$stmt = $db->prepare($sql);
		
		// execute the query
		$stmt->execute();
		
		// close the prepared statement
		$stmt->close();
		
	}
	
	public function update($fields = null) {
		
	}
	
	public function delete() {
		
	}
	
	private static function get_db_var_type($var) {
		
		$type = gettype($var);
		
		switch ($type) {
			
			case 'integer': case 'boolean':
				return 'i';
			
			case 'double':
				return 'd';
				
			case 'string': default:
				return 's';
			
		}
		
	}
	
	// functions below are for use of index operators (found on php.net)
	
	public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }
	
};

?>