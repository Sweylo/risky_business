<?php include '../view/header.php'; ?>

<h2>Database Setup</h2>

<div class="clear"></div>

<p class="error"><?php echo sql::$error_message; ?></p>

<form method="post" action="./" class="std-form">
	
	<input type="hidden" name="action" value="db_setup_save">

    <div>
		<input type="text" name="mysql_host" placeholder="MySQL hostname" 
			value="<?php echo $host; ?>">
	</div>
	<div>
		<input type="number" name="mysql_port"
			placeholder="MySQL port (default: 3306)" value="<?php echo $port; ?>">
	</div>
	<div>
		<input type="text" name="mysql_user" placeholder="MySQL username"
			value="<?php echo $user; ?>">
	</div>
	<div>
		<input type="text" name="mysql_password" placeholder="MySQL password"
			value="<?php echo $password; ?>">
	</div>
	<div>
		<input type="text" name="mysql_db" placeholder="MySQL DB name"
			value="<?php echo $db_name; ?>">
	</div>
	
	<input class="button start" type="submit" value="Save">

</form>

<?php include '../view/footer.php'; ?>
