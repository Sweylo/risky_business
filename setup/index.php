<?php

/**
 *	controller for the setup pages
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);
date_default_timezone_set('UTC');

//session_start();

$page = 'setup';
$is_db_setup_page = true;
$is_admin_setup_page = true;

require_once('../model/sql.php');
require_once('../model/input.php');
require_once('../model/log.php');
require_once('../model/err.php');
require_once('../model/user_db.php');

$action = (input(INPUT_GET, 'action', true) === null) 
	? input(INPUT_POST, 'action', true) 
	: input(INPUT_GET, 'action', true);

// display error if not admin user
if (sql::is_connected() && $me['user_name'] != 'admin' && !$admin_needs_pw) {
	$action = 'unauthorized';
}

switch ($action) {

	case 'db_setup': 
        
		if ($config) {
			$host = $config['mysql_host'];
			$user = $config['mysql_user'];
			$password = $config['mysql_password'];
			$db_name = $config['mysql_db'];
			$port = $config['mysql_port'];
		}
        
        
		
		include('db_setup_form.php');
		break;
	
	case 'db_setup_save':
		
		$db_config = array();
		
		$db_config['mysql_host'] = input(INPUT_POST, 'mysql_host');
		$db_config['mysql_user'] = input(INPUT_POST, 'mysql_user');
		$db_config['mysql_password'] = input(INPUT_POST, 'mysql_password', true);
		$db_config['mysql_db'] = input(INPUT_POST, 'mysql_db');
		$db_config['mysql_port'] = (input(INPUT_POST, 'mysql_port', true, FILTER_VALIDATE_INT)) 
			? input(INPUT_POST, 'mysql_port')
			: 3306;

		// encode the data to json and write it out to the file
		$config_file = fopen('../config/db_config.json', 'w');
		fwrite($config_file, json_encode($db_config, JSON_PRETTY_PRINT));
		fclose($config_file);
		
		header('Location: .?action=db_setup');
		break;
	
	case 'admin_setup': 
		
		$admin = get_user_by_name('admin');
		
		if (admin) {
			$username = $admin['user_name'];
			$email = $admin['user_email'];
		}
		
		include('admin_setup_form.php');
		break;
	
	case 'admin_setup_save':
		
		$email = input(INPUT_POST, 'email', false, FILTER_VALIDATE_EMAIL);
		$password = input(INPUT_POST, 'password');
		$confirm = input(INPUT_POST, 'confirm');
		
		if ($password != $confirm) {
			$error_message = 'Passwords do not match.';
			include('admin_setup_form.php');
		}
		
		try {
			edit_user(1, 'admin', $password, $email);
		} catch(mysqli_sql_exception $e) {
			$error_message = $e->getMessage();
			include('admin_setup_form.php');
		}
		
		header('Location: ../setup/?action=admin_setup');
		
		break;
		
	case 'user_manager':
		
		$users = get_users();
		
		include('user_manager.php');
		break;
		
	case 'unauthorized':
		err::out('You do not have permission to edit the setup.');
		break;
	
	default:
		include('setup_select.php');
	
}

?>