<?php include '../view/header.php'; ?>

<h2>User Manager</h2>

<table>
	
	<th width="20px">ID</th>
	<th>Username</th>
	
	<?php foreach ($users as $user) { ?>
	<tr>
		<td><?php echo $user['user_id']; ?></td>
		<td><?php echo $user['user_name']; ?></td>
	</tr>
	<?php } ?>
	
</table>

<?php include '../view/footer.php'; ?>
