<form method="post" action="./" class="std-form">
	
	<input type="hidden" name="action" value="<?php echo $form_action; ?>">

    <div>
		<label>Username</label>
		<?php if ($is_admin_setup) { ?>
		<input type="text" name="username" placeholder="admin" 
			value="<?php echo $username; ?>" disabled="true">
		<?php } else { ?>
		<input type="text" name="username" placeholder="Username" 
			value="<?php echo $username; ?>">
		<?php } ?>
	</div>
	<div>
		<label>E-mail</label>
		<input type="text" name="email" placeholder="Email address" 
			value="<?php echo $email; ?>">
	</div>
	
	<br />
	
	<div>
		<label>Password</label>
		<input type="password" name="password" placeholder="Password">
	</div>
	<div>
		<label>Confirm password</label>
		<input type="password" name="confirm" placeholder="Confirm">
	</div>
	
	<input class="button start" type="submit" value="Save">

</form>