		</div>
		
		<div class="clear"></div>
		
	</main>
	
	<footer>
		<p>
			<span>&copy;</span>
			<a target="blank">The Risky Business Team</a>
			<span>, <?php echo date('Y'); ?></span>
		</p>
	</footer>

</body>
