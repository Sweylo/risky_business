<?php

//require_once('../model/input.php');
//require_once('../model/user_db.php');

$login_error = input(INPUT_GET, 'login_error');

if ($login_error === USER_NOT_FOUND) {
	$login_error_message = 'User not found.';
} else if ($login_error === WRONG_PASSWORD) {
	$login_error_message = 'Wrong password.';
}

$home_class = ($page == 'home') ? 'current' : '';
$setup_class = ($page == 'setup') ? 'current' : '';
$map_class = ($page == 'map') ? 'current' : '';
$webgl_class = ($page == 'webgl') ? 'current' : '';

?>

<!DOCTYPE html>

<head>

	<title>Risky Business</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto|Slabo+27px" rel="stylesheet">
	<link href="../view/css/reset.css" type="text/css" rel="stylesheet" />
	<link href="../view/css/main.css" type="text/css" rel="stylesheet" />
	<script src="../js/jquery-3.2.1.min.js"></script>

	<?php if ($init_webgl) { ?>

	<!-- webgl stuff -->

	<link href="../view/css/map.css" type="text/css" rel="stylesheet" />
	<script src="../js/glMatrix-0.9.5.min.js"></script>
	<script src="../js/Shape.js"></script>
	<script src="../js/Square.js"></script>
	<script src="../js/HtmlIO.js"></script>
	<!--<script src="../webgl_test/tutorial_lesson01.js"></script>-->

	<script id="shader-fs" type="x-shader/x-fragment">
		varying lowp vec4 vColor;

		void main(void) {
			gl_FragColor = vColor;
		}
    </script>

	<script id="shader-vs" type="x-shader/x-vertex">
		attribute vec3 aVertexPosition;
		attribute vec4 aVertexColor;

		uniform mat4 uMVMatrix;
		uniform mat4 uPMatrix;

		varying lowp vec4 vColor;

		void main(void) {
			gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
			vColor = aVertexColor;
		}
    </script>



	<?php } ?>

</head>

<body <?php echo $init_webgl ? 'onload="webGLStart();" id="webgl"' : ''; ?>>

	<main>

		<aside>

			<!--<header>
				<h1><a href="../">Risky Business</a></h1>
			</header>-->

			<?php if ($me) { ?>

			<div id="user-panel">
				<h4>
					<?php echo $me['user_name']; ?>
					<img src="<?php echo get_gravatar($me['user_email'], 56); ?>">
				</h4>
				<p><a href="../user/?action=logout">Logout</a></p>
			</div>

			<?php } else { ?>

			<div id="login">

				<h4>Login</h4>

				<p class="error"><?php echo $login_error_message; ?></p>

				<?php if (sql::is_connected()) { ?>

				<?php if (!$admin_needs_pw) { ?>

				<form action="../user/" method="post" class="std-form">
					<input type="hidden" name="action" value="login">
					<div><input type="text" name="username" placeholder="Username"></div>
					<div><input type="password" name="password" placeholder="Password"></div>
					<div>
						<input type="submit" value="Login">
						<span>&nbsp;or&nbsp;</span>
						<a href="../user?action=register">Register</a>
					</div>
				</form>

				<?php } else { ?>

				<span>Admin account needs setup.</span>

				<?php }} else { ?>

				<span>No database connected.</span>

				<?php } ?>

			</div>

			<?php } ?>

			<nav>
				<ul>
					<li><a class="<?php echo $home_class; ?>" href="../main">Home</a></li>
					<?php if ($me['user_name'] == 'admin') { ?>
					<li><a class="<?php echo $setup_class; ?>" href="../setup">Setup</a></li>
					<?php } ?>
					<li><a class="<?php echo $map_class; ?>" href="../map">Maps</a></li>
					<li>
						<a class="<?php echo $webgl_class; ?>" href="../webgl_test">WebGL Test</a>
					</li>
				</ul>
			</nav>

		</aside>

		<div id="content">
