<?php

/**
 *	controller for webgl tests
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);
date_default_timezone_set('UTC');

session_start();

$page = 'webgl';
$init_webgl = true;

require_once('../model/sql.php');
require_once('../model/err.php');
require_once('../model/input.php');
require_once('../model/user_db.php');
require_once('../model/test_db.php');

$action = (input(INPUT_POST, 'action', true) === null) 
	? input(INPUT_GET, 'action', true) 
	: input(INPUT_POST, 'action', true);

switch ($action) {
	
	case 'load_test':
		$value = input(INPUT_POST, 'test_col');
		add_test($value);
		break;
		
	default:
		include('webgl_test.php');
	
}

?>