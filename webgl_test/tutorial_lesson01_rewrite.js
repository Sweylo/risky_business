// globals
var thePast = 0;
var angle = 0;
var mouseDown = false;
var lastMouseX = 0;
var lastMouseY = 0;
var testArray = [];

// global shapes
var triangle;
var square;
var pentagon;
var userDraw;

// globals inherited from Shape.js (must be called before this)
var canvas = canvas;
var gl = gl;
var ratio = ratio;
var mat4 = mat4;
var mvMatrix = mvMatrix;
var pMatrix = pMatrix;

// constants
var MOUSE_LEFT = 1;
var MOUSE_RIGHT = 2;
var MOUSE_MIDDLE = 4;
var ASIDE_WIDTH = 241;

// draw the objects
function drawScene() {
	
	/*
	 * initialize objects to be drawn
	 */
	
	triangle = new Shape([
		-2.1,  1.0,
		-3.1, -1.0,
		-1.1, -1.0
	], gl.TRIANGLES, [0.5,  0.0,  0.0]);
	
	square = new Shape([
		-1.0,  1.0,
		 1.0,  1.0,
		 1.0, -1.0,
		-1.0, -1.0
	], gl.TRIANGLE_FAN, [0.0,  0.5,  0.0]);
	
	pentagon = new Shape([
		 2.1,  1.0,
		 3.1,  0.0,
		 3.1, -1.0,
		 1.1, -1.0,
		 1.1,  0.0
	], gl.TRIANGLE_FAN, [0.0,  0.0,  0.5]);
	
	userDraw = new Shape(
		$('#testDraw').prop('checked') 
			? testArray.concat(getPoint(lastMouseX - ASIDE_WIDTH, canvas.height - lastMouseY))
			: testArray.concat(getPoint(testArray[testArray.length - 1] - ASIDE_WIDTH, 
				canvas.height - testArray[testArray.length - 0])), 
	gl.TRIANGLE_FAN, [0.5, 0.5, 0.5]);
	
	/*
	 * draw the objects
	 */
	
	// clear the screen
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	// resize canvas accordingly
	resize();
	
	// get values from the control form
	var transX = $('#transX').val();
	var transY = $('#transY').val();
	//var rotX = $('#rotX').val();
	//var rotY = $('#rotY').val();
	var zoom = -$('#zoom').val();
	var fov = $('#fov').val();
	
	// set the viewport dimensions
	gl.viewport(0, 0, canvas.width, canvas.height);

	// setup the camera
	mat4.perspective(
		fov,									// field of view
		canvas.width / canvas.height,			// aspect ratio
		0.1,									// z-near
		100.0,									// z-far
		pMatrix
	);
	
	// initialize the matrix stack
	mat4.identity(mvMatrix);

	// perform tranformations
	mat4.translate(mvMatrix, [transX * ratio, transY * ratio, zoom]);
	//mat4.rotate(mvMatrix, degToRad(rotX), [0, 1, 0]);
	//mat4.rotate(mvMatrix, degToRad(rotY), [1, 0, 0]);
	
	// animate rotation of the figure
	//mat4.rotate(mvMatrix, degToRad(angle), [0, 0, 1]);
	
	// try to draw the objects
	try {
		
		mvPushMatrix();
			
			// program-drawn shapes
			triangle.draw();
			square.draw();
			pentagon.draw();
			
			// user-drawn shape
			userDraw.draw();

		mvPopMatrix();
	
	} catch (e) {
		console.log('error drawing shapes');
	}
	
}

function resize() {

	// check if the canvas is not the same size
	if (canvas.width !== canvas.clientWidth || canvas.height !== canvas.clientHeight) {
		canvas.width = canvas.clientWidth;
		canvas.height = canvas.clientHeight;
	}
	
	//console.log('size | (' + canvas.width + ', ' + canvas.height + ')');
	
	initGL();
	
}

function handleMouseDown(event) {
	
	canvas.style.cursor = "move";
	
	mouseDown = event.buttons;
	lastMouseX = event.clientX;
	lastMouseY = event.clientY;
	
	//console.log(event);
	
	var point = getPoint(lastMouseX - ASIDE_WIDTH, canvas.height - lastMouseY);
	
	console.log("click | (" + (lastMouseX - ASIDE_WIDTH) + ", " + (canvas.height - lastMouseY) + ")");
	console.log("drawLocation | (" + point[0] + ", " + point[1] + ")");
	
}

function handleMouseUp(event) {
	
	canvas.style.cursor = "pointer";
	mouseDown = false;
	
	if ($('#testDraw').prop('checked')) {
		testArray = testArray.concat(getPoint(event.clientX - ASIDE_WIDTH, canvas.height - event.clientY));
	}
	
	//console.log(test);
	//console.log(testArray);
	
}

function handleMouseMove(event) {
	
	if (!mouseDown && !$('#testDraw').prop('checked')) {
		return;
	}
	
	var newX = event.clientX;
	var newY = event.clientY;
	var deltaX = newX - lastMouseX;
	var deltaY = newY - lastMouseY;
	
	//console.log("delta | (" + deltaX + ", " + deltaY + ")");
	
	switch (mouseDown) {
		
		case MOUSE_LEFT:
			$('#transX').val(parseInt($('#transX').val()) + deltaX);
			$('#transY').val(parseInt($('#transY').val()) - deltaY);
			break;
			
		case MOUSE_RIGHT:
			$('#rotX').val(parseInt($('#rotX').val()) + deltaX);
			$('#rotY').val(parseInt($('#rotY').val()) + deltaY);
			break;
			
	}
	
	lastMouseX = newX;
	lastMouseY = newY;
	
}

function handleMouseScroll(event) {
	
	//console.log(event);
	
	var step = 0.5;

	switch(event.deltaY) {
		
		case -100:	// scroll up
			if (parseInt($('#zoom').val()) > 0) {
				$('#zoom').val(parseFloat($('#zoom').val()) - step);
			}
			break;
			
		case 100:	// scroll down
			if (parseInt($('#zoom').val()) < 99) {
				$('#zoom').val(parseFloat($('#zoom').val()) + step);
			}
			break;
	}

}

// performs increments of animation variables
function animate() {
	
	var thePresent = new Date().getTime();
	
	if (thePast !== 0) {
		//angle += (thePresent - thePast) / 10;
	}
	
	thePast = thePresent;
	
}

// responsible for animation
function tick() {
	requestAnimationFrame(tick);
	drawScene();
	animate();
}

// calls all functions to get webgl running
function webGLStart() {
	
	// get canvas object from the html element
	canvas = document.getElementById("map-canvas");
	
	// set cursor to indicate loading
	canvas.style.cursor = 'wait';
	
	// initialization
	initGL();
	initShaders();
	
	// disable right click menu
	$(canvas).bind('contextmenu', function(e) {
		return false;
	}); 
	
	// prevent default action for middle mouse button
	$(canvas).on('click', function(e) { 
		if(e.which === 2) {
			e.preventDefault();
		}
	});
	
	// show controls and set cursor to pointer
	canvas.style.cursor = "pointer";
	$('.control').show();
	$('.control-content').hide();

	// set background color and enable depth testing
	gl.clearColor(1.0, 1.0, 1.0, 1.0);
	gl.enable(gl.DEPTH_TEST);
	
	// assign event handling functions
	canvas.onmousedown = handleMouseDown;
    canvas.onmouseup = handleMouseUp;
    canvas.onmousemove = handleMouseMove;
	canvas.addEventListener('wheel', handleMouseScroll, false);
	
	tick();
	
}