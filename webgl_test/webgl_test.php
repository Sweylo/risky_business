<?php include('../view/header.php'); ?>

<script src="../webgl_test/tutorial_lesson01_rewrite.js"></script>
<script src="../webgl_test/jquery.map_control.js"></script>

<div id="map">
	
	<h4>Loading...</h4>
	
	<canvas id="map-canvas"></canvas>
	
	<div class="control" id="primary">
		
		<div class="control-header">
			<h4>WebGL Control</h4>
			<button class="hide on">Show</button>
			<div class="clear"></div>
		</div>
		
		<div class="control-content">
			
			<table>
				
				<tr>
					<td>transX</td>
					<td><input id="transX" type="number" name="transX" value="0"></td>
					<td>transY</td>
					<td><input id="transY" type="number" name="transY" value="0"></td>
				</tr>
				
				<tr>
					<td>rotX</td>
					<td><input id="rotX" type="number" name="rotX" value="0" disabled></td>
					<td>rotY</td>
					<td><input id="rotY" type="number" name="rotY" value="0" disabled></td>
				</tr>
				
				<tr>
					<td>Zoom</td>
					<td><input id="zoom" type="number" name="zoom" min="1" max="100" value="6"></td>
					<td>Draw</td>
					<td><input id="testDraw" type="checkbox" name="testDraw"></td>
				</tr>
				
				<tr>
					<td>FOV</td>
					<td><input id="fov" type="number" name="fov" min="1" value="53" disabled></td>
				</tr>
				
			</table>
			
			<button id="reset">Reset</button>
			<!--<button id="loadTest">Load test</button>-->
			
			<div id="ajax"></div>
			
		</div>
		
	</div>
	
</div>

<?php include('../view/footer.php'); ?>